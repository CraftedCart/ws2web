import Vue from 'vue';
import App from './App.vue';
import Buefy from 'buefy';
import router from './router';

Vue.use(Buefy);

Vue.config.productionTip = false;

window.vm = new Vue({
  data: {
    ver: "nyaa",
    message: "nyaa",
  },

  beforeCreate: async () => {
    const response = await fetch("https://gitlab.com/api/v4/projects/6719192/repository/commits");
    const json = await response.json(); //Extract JSON from the http response
    window.devCommits = json;
  },

  router,
  render: h => h(App)
}).$mount('#app');
